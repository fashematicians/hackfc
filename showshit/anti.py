__author__ = 'Carl'
from showshit.event import Event
from showshit.constants import ANTI_FILE
import pygame


class Anti(Event):

    image = pygame.image.load(ANTI_FILE)

    def __init__(self, lat, lon):
        super(Anti, self).__init__(lat, lon)
        self.color = (0, 0, 255)
        self.image = ANTI_FILE

    def draw(self, screen, pos, scale):
        out_image = pygame.transform.smoothscale(Anti.image, (int(Event.IMX*scale), int(Event.IMY*scale)))
        screen.blit(out_image, pos)