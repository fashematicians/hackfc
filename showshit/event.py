__author__ = 'Carl'
import pygame


class Event(object):

    IMX = 32
    IMY = 32

    def __init__(self, lat, lon):
        self.lat = lat
        self.lon = lon
        self.image = None

    def draw(self, screen, pos, scale):
        in_image = pygame.image.load(self.image)
        out_image = pygame.transform.smoothscale(in_image, (Event.IMX*scale, Event.IMY*scale))
        screen.blit(out_image, pos)
