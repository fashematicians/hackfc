__author__ = 'Carl'


class Map:

    def __init__(self, file, w, h, lon, lat):
        self.w = w
        self.h = h
        self._lon = lon
        self._lat = lat
        self._xmin = min(lon)
        self._ymin = min(lat)
        self._xscale = (max(lon)-min(lon))/w
        self._yscale = (max(lat)-min(lat))/h
        self._file = file

    def gps_to_x(self, lon):
        """ Take a given gps coord and translate to map (x,y)
        """
        x = (lon-self._xmin)/self._xscale

        return int(x)

    def gps_to_y(self, lat):
        """ Take a given gps coord and translate to map (x,y)
        """
        y = (0 - ((lat-self._ymin)/self._yscale)) + self.h

        return int(y)
