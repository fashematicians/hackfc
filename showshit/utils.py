__author__ = 'Carl'
import string

def get_lat_long(file):
    """
    Take a csv data file from the FC data and output lon,lat
    """

    x = []
    y = []
    fi = open(file, 'r')

    linenum = 0
    for line in fi:
        if linenum > 0:
            try:
                fields = string.split(line, ',')
                lat, lon = fields[2:4]
                x.append(float(lon))
                y.append(float(lat))
            except:
                print "error!"
        linenum += 1
    fi.close()

    return x, y