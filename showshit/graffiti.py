__author__ = 'Carl'
from showshit.event import Event
from showshit.constants import GRAFFITI_FILE
import pygame


class Graffiti(Event):

    image = pygame.image.load(GRAFFITI_FILE)

    def __init__(self, lat, lon):
        super(Graffiti, self).__init__(lat, lon)
        self.color = (0, 255, 0)
        self.image = GRAFFITI_FILE

    def draw(self, screen, pos, scale):
        out_image = pygame.transform.smoothscale(Graffiti.image, (int(Event.IMX*scale), int(Event.IMY*scale)))
        screen.blit(out_image, pos)