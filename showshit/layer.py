__author__ = 'Carl'
import pygame
from showshit.graffiti import Graffiti
from showshit.shit import Shit
from showshit.anti import Anti
from showshit.constants import SHIT_FILE, GRAFFITI_FILE, ANTI_FILE
import pygame


class DataLayer():

    def __init__(self, id, events, map, dow):
        self.id = id
        self.events = events
        self.map = map
        self.dow = dow

    def draw(self, screen, viewportpos, scale):
        for i in range(0, len(self.events)):
            eve = self.events[i]
            x = self.map.gps_to_x(eve.lon)
            y = self.map.gps_to_y(eve.lat)
            pos = (int(viewportpos.x+(x*scale)), int(viewportpos.y+(y*scale)))
            #pygame.draw.circle(screen, eve.color, pos, int(15*scale))
            eve.draw(screen, pos, scale)

    def get_event_types(self):
        shits, grafs, antis = 0, 0, 0
        for e in self.events:
            if isinstance(e, Shit):
                shits += 1
            elif isinstance(e, Graffiti):
                grafs += 1
            elif isinstance(e, Anti):
                antis += 1
        return shits, grafs, antis