#!/usr/bin/python

import numpy as np
import random
import cPickle

def load_matrices():
    prob_matrices = {}

    with open('dog.prob', 'rb') as fp:
        prob_matrices['dog'] = cPickle.load(fp)

    with open('asb.prob', 'rb') as fp:
        prob_matrices['asb'] = cPickle.load(fp)

    with open('grf.prob', 'rb') as fp:
        prob_matrices['grf'] = cPickle.load(fp)

    return prob_matrices


prob_matrices = load_matrices()
event_types = ['dog', 'asb', 'grf']


def generate_street_events(day_of_week, event_type):
    event_matrix = prob_matrices[event_type]['matrix']

    street_probs = event_matrix[day_of_week,]
    random_probs = np.random.random((len(street_probs),))

    triggered_events = random_probs > street_probs
    return prob_matrices[event_type]['streets'][triggered_events]


def most_probable_events():
    most_probable = {}

    for dow in range(0, 6):
        for event_type in event_types:
            event_row = prob_matrices[event_type]['matrix'][dow,]
            street_names = prob_matrices[event_type]['streets']

            while True:
                street_idx = np.argmax(event_row)
                most_probable_street = street_names[street_idx]

                if most_probable_street == 'Not Recorded':
                    event_row[street_idx] = 0
                else:
                    break

            if dow not in most_probable:
                most_probable[dow] = {}

            most_probable[dow][event_type] = {
                'street': most_probable_street,
                'p': event_row[street_idx],
            }

    return most_probable


output = []

for step in range(365):
    day_of_week = step % 7
    events = []

    for event_type in event_types:
        for s in generate_street_events(day_of_week, event_type):
            events += [{'dow': day_of_week, 'event': event_type, 'street': s}]

    # we generate events for ALL days across the year so
    # here we need to sample for a single day. The number
    # to sample is determined by the number of events in the
    # original dataset

    day_counts = [1101, 1009, 1080, 943, 917, 628, 476]
    day_normalisation = 10

    normalised_day_count = day_counts[day_of_week]/day_normalisation
    event_number = int(random.normalvariate(normalised_day_count, day_normalisation*2))

    for subevent in random.sample(events, max(event_number, len(events))):
        output.append(subevent)

    print day_of_week, event_number

with open('generated_events.pk', 'wb') as fp:
    cPickle.dump(output, fp)

with open('most_probable.pk', 'wb') as fp:
    mp = most_probable_events()
    cPickle.dump(mp, fp)
    print mp

