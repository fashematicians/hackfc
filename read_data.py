import numpy as np
import pandas
import cPickle

def binarize(boolean):
    if boolean:
        return 1
    else:
        return 0

dog = pandas.read_csv('./data/Dog', index_col = 0, parse_dates=[0])
asb = pandas.read_csv('./data/AntiSocialBehavior', index_col = 0, parse_dates=[0])

asb['blood'] = asb['blood'].apply(binarize)
asb['humanfouling'] = asb['humanfouling'].apply(binarize)
asb['vomit'] = asb['vomit'].apply(binarize)
asb['urine'] = asb['urine'].apply(binarize)

graffiti = pandas.read_csv('./data/Graffiti', index_col = 0, parse_dates=[0])

########



########



dog_per_weekday_street_counts = dog.groupby([lambda x: x.weekday, dog['street']]).count()['street']
asb_per_weekday_street_counts = asb.groupby([lambda x: x.weekday, asb['street']]).sum()[['urine','blood','humanfouling','vomit']]
grf_per_weekday_street_counts = graffiti.groupby([lambda x: x.weekday, graffiti['street']]).count()['street']


###### dog matrix
#_,streets = set(zip(*dog_per_weekday_street_counts.index.values))
#midx = pandas.MultiIndex.from_tuples(dog_per_weekday_street_counts.index.values, names=['day', 'street'])
#dog_matrix = pandas.DataFrame(dog_per_weekday_street_counts.values, index=midx)[0].unstack()
#dog_matrix_streets = dog_matrix.columns

def extract_matrix(df):
    """Returns a tuple matrix of shape (7, number of streets) containing counts of events in cells and column names (streets) """
    _,streets = set(zip(*df.index.values))
    midx = pandas.MultiIndex.from_tuples(df.index.values, names=['day', 'street'])
    matrix = pandas.DataFrame(df.values, index=midx)[0].unstack().fillna(0)
    return {'matrix': matrix.values, 'columns': matrix.columns}

def prob_matrix(m):
    p = (m.T / np.sum(m, axis=1, dtype=np.float32)).T
    return np.absolute(p + np.random.normal(scale=0.00001, size=p.shape))

d = extract_matrix(dog_per_weekday_street_counts)
a = extract_matrix(asb_per_weekday_street_counts)
g = extract_matrix(grf_per_weekday_street_counts)

street_map = {}

def map_streets(df, streets):
    for each in streets:
        latlong_pair = tuple(df[df['street'] == each][['lat', 'long']].mean())
        street_map[each] = latlong_pair

    return street_map

map_streets(dog, d['columns'])
map_streets(asb, a['columns'])
map_streets(graffiti, g['columns'])

dog_count = extract_matrix(dog_per_weekday_street_counts)
asb_count = extract_matrix(asb_per_weekday_street_counts)
grf_count = extract_matrix(grf_per_weekday_street_counts)


dog_prob = prob_matrix(dog_count['matrix'])
asb_prob = prob_matrix(asb_count['matrix'])
grf_prob = prob_matrix(grf_count['matrix'])


with open('dog.prob', 'wb') as fp:
    cPickle.dump({'matrix': dog_prob, 'streets': dog_count['columns']}, fp)

with open('asb.prob', 'wb') as fp:
    cPickle.dump({'matrix': asb_prob, 'streets': asb_count['columns']}, fp)

with open('grf.prob', 'wb') as fp:
    cPickle.dump({'matrix': grf_prob, 'streets': grf_count['columns']}, fp)
