__author__ = 'Carl'
import pickle
from showshit.shit import Shit
from showshit.graffiti import Graffiti
from showshit.anti import Anti
from showshit.layer import DataLayer
from showshit.constants import BASE_PROB


dows = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']


def get_street_map():
    return pickle.load(open('street_map.pkl'))

def get_street_probabilities():
    return pickle.load(open('simulation/most_probable.pk'))

def get_prob_labels():
    street_map = get_street_map()
    street_prob = get_street_probabilities()
    base_prob = 1/len(street_map.keys())
    for k in street_prob.keys():
        street_prob[k]['grf']['l'] = street_prob[k]['grf']['p'] / BASE_PROB
        street_prob[k]['asb']['l'] = street_prob[k]['asb']['p'] / BASE_PROB
        street_prob[k]['dog']['l'] = street_prob[k]['dog']['p'] / BASE_PROB
    return street_prob

def import_events(file, map):
    """ unpickle eddies list of events, and store into layers.
        One layer per "day"
    """

    layers = []
    current_day = 0
    events = []
    count = 0
    street_map = get_street_map()

    event_list = pickle.load(open(file))
    for event in event_list:
        e = None
        (long, lat) = street_map[event['street']]
        if event['event'] == 'dog':
            e = Shit(long, lat)
        elif event['event'] == 'grf':
            e = Graffiti(long, lat)
        elif event['event'] == 'asb':
            e = Anti(long, lat)

        if event['dow'] == current_day:
            events.append(e)
        else:
            layers.append(DataLayer("Day: %d '%s'" % (count, dows[count % 7]), events, map, current_day))
            current_day = event['dow']
            events = []
            count += 1

    return layers