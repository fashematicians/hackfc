__author__ = 'Carl'

from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import string


def get_lat_long(file):
    """
    Take a csv data file from the FC data and output lon,lat
    """

    x = []
    y = []
    fi = open(file, 'r')

    linenum = 0
    for line in fi:
        if linenum > 0:
            try:
                fields = string.split(line, ',')
                lat, lon = fields[2:4]
                x.append(float(lon))
                y.append(float(lat))
            except:
                print "error!"
        linenum += 1
    fi.close()

    return x, y



image = plt.imread('london.png')

(dx, dy) = get_lat_long('Dog.csv')#
#(gx, gy) = get_lat_long('Graffiti.csv')
#(ax, ay) = get_lat_long('AntiSocialBehavior.csv')

lonmin = -0.2182
lonmax = -0.0997
latmin = 51.4813
latmax =51.5446

m = Basemap(llcrnrlon=lonmin, llcrnrlat=latmin, urcrnrlon=lonmax, urcrnrlat=latmax, lat_ts=20,
            resolution='h', projection='merc', lon_0=0, lat_0=51.4)

extentx, extenty = m([lonmin, lonmax], [latmin, latmax])
implot = plt.imshow(image, extent=[extentx[0], extentx[1], extenty[0], extenty[1]])

dx, dy = m(dx, dy)
#gx, gy = m(gx, gy)
#ax, ay = m(ax, ay)


m.scatter(dx[1:10], dy[1:10], s=50, c='r', marker='o', alpha=1.0)
#m.scatter(gx, gy, s=50, c='b', marker='o', alpha=1.0)
#m.scatter(ax, ay, s=50, c='g', marker='o', alpha=1.0)


plt.title('Shit in London')
plt.show()
