__author__ = 'Carl'

from showshit.map import Map
from showshit.shit import Shit
from showshit.graffiti import Graffiti
from showshit.anti import Anti
from showshit.layer import DataLayer
from showshit.constants import MAP_FILE
from showshit.utils import get_lat_long
import sys
import pygame
import random
from pygame.locals import *
from create_event_layers import import_events, get_prob_labels

pygame.init()

RES_X = 1366
RES_Y = 768

IMAGE_W = 2137
IMAGE_H = 1842


def _init_map():
    # Load map
    map = Map(MAP_FILE, 2137, 1842, [-0.2182, -0.0997], [51.4813, 51.5446])
    return map

def _show_layer_stats(layer, screen, steet_prob):
    (ss, gs, ass) = layer.get_event_types()
    font = pygame.font.SysFont('Arial', 36)
    dstext = font.render("Dog shit: %d" % (ss), 1, (255, 0, 0))
    gtext = font.render("Graffiti: %d" % (gs), 1, (0, 255, 0))
    atext = font.render("AntiSocials: %d" % (ass), 1, (0, 0, 255))
    idtext = font.render("%s" % layer.id, 1, (0, 0, 0))

    lfont = pygame.font.SysFont('Arial', 24)
    likelylabel = lfont.render("Most likely areas:", 1, (0, 0, 0))
    likelyshitlabel = lfont.render("Shit: %s (%dx)" % (steet_prob[layer.dow]['dog']['street'], steet_prob[layer.dow]['dog']['l']), 1, (255, 0, 0))
    likelygraflabel = lfont.render("Graffiti: %s (%dx)" % (steet_prob[layer.dow]['grf']['street'], steet_prob[layer.dow]['grf']['l']), 1, (0, 255, 0))
    likelyantilabel = lfont.render("ASB: %s (%dx)" % (steet_prob[layer.dow]['asb']['street'], steet_prob[layer.dow]['asb']['l']), 1, (0, 0, 255))


    dstextpos = dstext.get_rect()
    dstextpos.x += 10
    dstextpos.y += 10
    gtextpos = gtext.get_rect()
    gtextpos.x += 10
    gtextpos.y += dstextpos.y + dstextpos.h + 10
    atextpos = atext.get_rect()
    atextpos.x += 10
    atextpos.y += gtextpos.y + gtextpos.h + 10

    idtextpos = idtext.get_rect()
    idtextpos.x = screen.get_rect().w - idtextpos.w - 10

    likelylabelpos = likelylabel.get_rect()
    likelylabelpos.x = screen.get_rect().w - likelylabelpos.w - 10
    likelylabelpos.y = screen.get_rect().centery + 200
    likelyshitlabelpos = likelyshitlabel.get_rect()
    likelyshitlabelpos.x = screen.get_rect().w - likelyshitlabelpos.w - 10
    likelyshitlabelpos.y = likelylabelpos.y + likelylabelpos.h + 10
    likelygraffitilabelpos = likelygraflabel.get_rect()
    likelygraffitilabelpos.x = screen.get_rect().w - likelygraffitilabelpos.w - 10
    likelygraffitilabelpos.y = likelyshitlabelpos.y + likelyshitlabelpos.h + 10
    likelyantilabelpos = likelyantilabel.get_rect()
    likelyantilabelpos.x = screen.get_rect().w - likelyantilabelpos.w - 10
    likelyantilabelpos.y = likelygraffitilabelpos.y + likelygraffitilabelpos.h + 10

    likelybbx = min([likelylabelpos.x, likelyshitlabelpos.x, likelygraffitilabelpos.x, likelyantilabelpos.x])
    likelybby = min([likelylabelpos.y, likelyshitlabelpos.y, likelygraffitilabelpos.y, likelyantilabelpos.y])
    likelybbw = max([likelylabelpos.w, likelyshitlabelpos.w, likelygraffitilabelpos.w, likelyantilabelpos.w])
    likelybbh = likelylabelpos.h * 4 + 50

    pygame.draw.rect(screen, (245, 245, 220), (0, 0, 250, 180))
    pygame.draw.rect(screen, (245, 245, 220), (idtextpos.x - 10, idtextpos.y-20, idtextpos.w + 20, idtextpos.h + 20))
    pygame.draw.rect(screen, (245, 245, 220), (likelybbx - 10, likelybby-10, likelybbw + 20, likelybbh + 20))

    screen.blit(dstext, dstextpos)
    screen.blit(gtext, gtextpos)
    screen.blit(atext, atextpos)
    screen.blit(idtext, idtextpos)
    screen.blit(likelylabel, likelylabelpos)
    screen.blit(likelyshitlabel, likelyshitlabelpos)
    screen.blit(likelygraflabel, likelygraffitilabelpos)
    screen.blit(likelyantilabel, likelyantilabelpos)

#create the screen
screen = pygame.display.set_mode((RES_X, RES_Y), pygame.FULLSCREEN|pygame.HWSURFACE|pygame.DOUBLEBUF)


map = _init_map()

layers = import_events('simulation/generated_events.pk', map)
street_prob = get_prob_labels()

#events = []
#
#
#sx, sy = get_lat_long('data/Dog')
#num_poos = len(sx)
#for i in range(0, num_poos):
#    events.append(Shit(sy[i], sx[i]))
#
#gx, gy = get_lat_long('data/Graffiti')
#num_graf = len(gx)
#for i in range(0, num_graf):
#    events.append(Graffiti(gy[i], gx[i]))
#
#ax, ay = get_lat_long('data/AntiSocialBehavior')
#num_anti = len(ax)
#for i in range(0, num_anti):
#    events.append(Anti(ay[i], ax[i]))
#
#random.shuffle(events)
#
#layers = []
#for n in range(1, 8):
#    layers.append(DataLayer("Day: %d" % n, events[((n-1)*num_poos/10):(n*num_poos/10)], map))
currentlayer = 0


image = pygame.image.load(MAP_FILE)
imagePos = pygame.Rect((0, 0), (0, 0))

scale = 1
LeftButton = 0
WheelUp = 4
WheelDown = 5
while 1:
    for e in pygame.event.get():
        if e.type == QUIT: exit(0)
        if e.type == MOUSEMOTION:
            if e.buttons[LeftButton]:
                #clicked and moving
                rel = e.rel
                imagePos.x += rel[0]
                imagePos.y += rel[1]
        if e.type == MOUSEBUTTONDOWN:
            if e.button == WheelUp:
                scale *= 1.1
            if e.button == WheelDown:
                scale *= 0.9

        if e.type == KEYUP:
            if e.key == K_SPACE:
                currentlayer += 1
                if currentlayer > len(layers) -1:
                    currentlayer = 0
            if e.key == K_ESCAPE:
                sys.exit(0)
        if e.type == KEYDOWN:
            if e.key == K_LEFT:
                imagePos.x += 50
            elif e.key == K_RIGHT:
                imagePos.x -= 50
            elif e.key == K_UP:
                imagePos.y += 50
            elif e.key == K_DOWN:
                imagePos.y -= 50
            if e.key == K_k:
                scale *= 1.1
            if e.key == K_j:
                scale *= 0.9
            if e.key == K_r:
                scale = 1.0


        if imagePos.x > 0:
            imagePos.x = 0
        if imagePos.y > 0:
            imagePos.y = 0
        if imagePos.y < -1*map.h+RES_Y:
            imagePos.y = -1*map.h+RES_Y
        if imagePos.x < -1*map.w+RES_X:
            imagePos.x = -1*map.w+RES_X
        if scale < 0.43:
            scale = 0.43
        elif scale > 1.2:
            scale = 1.2


    # DRAW
    screen.fill(0)

    image = pygame.image.load(MAP_FILE)
    image = pygame.transform.smoothscale(image, (int(IMAGE_W*scale), int(IMAGE_H*scale)))

    # CURRENT EVENT LAYER
    layers[currentlayer].draw(image, pygame.Rect((0, 0), (0, 0)), scale)

     # MAP
    screen.blit(image, imagePos)

    #layers[currentlayer].draw(screen, imagePos, scale)

    # STATS
    _show_layer_stats(layers[currentlayer], screen, street_prob)

    pygame.display.flip()

    pygame.time.delay(30)
